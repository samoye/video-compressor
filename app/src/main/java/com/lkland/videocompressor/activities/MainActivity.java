package com.lkland.videocompressor.activities;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.lkland.util.FileUtils;
import com.lkland.util.Logger;
import com.lkland.videocompressor.R;
import com.lkland.videocompressor.fragments.QueueListFragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;



public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FileUtils.createFolderInExternalStorageDirectory("VideoCompressor");
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().add(R.id.container, new QueueListFragment()).commit();
		}
	}

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data); 
	}

	@Override
	public void onBackPressed() {
		if(getFragmentManager().getBackStackEntryCount()!=0)
			getFragmentManager().popBackStack();
	}
}
